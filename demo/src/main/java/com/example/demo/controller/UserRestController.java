package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.pojo.User;
import com.example.demo.service.UserService;

@RestController
public class UserRestController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/findAll", method = RequestMethod.GET)
	public List<User> getAllUser(){
		return userService.findAll();
	}

	@RequestMapping(value = "/saveUser", method = RequestMethod.POST)
	public User saveUser(@RequestBody User user){
		return this.userService.saveUser(user);
	}

	@RequestMapping(value = "/deleteAllUser", method = RequestMethod.DELETE)
	public void deleteAllUser() {
		this.userService.deleteAllUser();
	}
}
