package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.pojo.User;
import com.example.demo.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepository;
	
	@Override
	@Transactional
	public List<User> findAll() {
		return this.userRepository.findAll();
	}

	@Override
	@Transactional
	public User saveUser(User user) {
		return this.userRepository.save(user);
	}
	
	@Override
	@Transactional
	public void deleteAllUser() {
		this.userRepository.deleteAll();
	}
}
