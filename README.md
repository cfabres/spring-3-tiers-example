**Spring 3 tiers example**

The idea of this project is build a project with the tipically three tiers.

used the following site: [](https://start.spring.io/)
and were added teh following dependencies:
+ spring web
+ spring data JPA
+ mysql driver

Built thru spring you can find:
- mysql 
- pojo
- repository
- service
- Controller (Rest Controller)
    with three Methods (GET, POST and DELETE)

